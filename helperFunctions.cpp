#include <stdexcept>
#include <sstream> // Stringstream
#include <iostream>
#include <iomanip> // setw and setfill
#include "testDefines.hpp"

static int ret;

const void readCheck(uint32_t returnValue, uint32_t addr, uint32_t value, std::string regName){
    if(returnValue){ // -1 on failure
        std::stringstream sstream;
        sstream << std::hex << "Failed to read register " << std::setfill('0') << std::setw(2) << regName << " at offset: 0x" << addr;
        throw std::runtime_error(sstream.str());
    } else if(OUTPUTTEST)
        std::cout << std::hex << std::left << std::setfill(' ') << std::setw(40) << "Read:" << std::left << std::setfill(' ') << std::setw(40) << regName << ": 0x" << std::right << std::setfill('0') << std::setw(2) << value << std::endl;
}

const void writeCheck(uint32_t returnValue, uint32_t addr, uint32_t value, std::string regName){
    if(returnValue) {// -1 on failure
        std::stringstream sstream;
        sstream << std::hex << "Failed to write register " << regName << " at offset: 0x" << addr << " with return value: " << ret;
        throw std::runtime_error(sstream.str());
    } else if(OUTPUTTEST){
        std::cout << std::hex <<  std::left << std::setfill(' ') << std::setw(40) << "Wrote:" << std::left << std::setfill(' ') << std::setw(40) << regName << ": 0x" << std::right << std::setfill('0') << std::setw(2) << value << std::endl;
    }
}

const void printMainHeader(std::string header){
    #if PRINT_HEADERS
        const int hashAmount = MAIN_HEADER_HASH;
        const int headerSize = header.size();
        for (size_t i = 0; i < hashAmount/2 - headerSize/2; i++)
        {
            std::cout << "#";
        }
        std::cout << " " << header << " ";
        for (size_t i = 0; i < hashAmount/2 - headerSize/2; i++)
        {
            std::cout << "#";
        }
        std::cout << "\n" << std::endl;
    #endif
}

const void printSubHeader(std::string header){
    #if PRINT_HEADERS
        const int hashAmount = SUB_HEADER_HASH;
        const int headerSize = header.size();
        for (size_t i = 0; i < hashAmount/2 - headerSize/2; i++)
        {
            std::cout << "#";
        }
        std::cout << " " << header << " ";
        for (size_t i = 0; i < hashAmount/2 - headerSize/2; i++)
        {
            std::cout << "#";
        }
        std::cout << std::endl;
    #endif
}

uint32_t isolateBitsRead(uint32_t value, uint32_t bitWidth, uint8_t bitOffset){
    uint32_t bitMask = (uint64_t(1) << bitWidth) -1; // 1's on the first 'bitWidth' fields.
    bitMask = bitMask << bitOffset;
    return ((value & bitMask) >> bitOffset);
}

uint32_t isolateBitsWrite(uint32_t value, uint32_t bitWidth, uint8_t bitOffset, uint32_t oldValue){
    uint32_t bitMask = (uint64_t(1) << bitWidth) -1; // 1's on the first 'bitWidth' fields.
    bitMask = bitMask << bitOffset;
    return((oldValue & (~bitMask)) | (value<<bitOffset)); // Return oldvalue with the correct bits replaced.
}
