#pragma once
#include "libuio.h"
#include <vector>
#include "testDefines.hpp"


uio_info_t* initUio(const std::string& uioName);
void setupUio(uio_info_t* uio);
void closeUio(uio_info_t* uio);

void initTest(uio_info_t* uio, std::vector<uint32_t>values);

bool readAndCheckICRxFifo(uio_info_t* uio, std::vector<uint32_t>writtenValues);
std::vector<uint32_t> readICRxFifo(uio_info_t* uio);
void writeLpgbtFifo(uio_info_t* uio);
void readLpgbtRegisters(uio_info_t* uio, uint32_t readAmount);

uint32_t readStatusRegister(uio_info_t* uio);

#if TEST_INTR
    void waitIntr(uio_info_t* uio, int waitDurationMs);
    void enableIntr(uio_info_t* uio, uint32_t interruptEnableValue, uint32_t fieldWidth, uint32_t fieldOffset);
    void clearIntr(uio_info_t* uio, uint32_t interruptClearValue,uint32_t fieldWidth, uint32_t fieldOffset);
    void disableIntr(uio_info_t* uio);
#endif
