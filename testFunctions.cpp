#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include <algorithm>

#include "testFunctions.hpp"
#include "regInfo.hpp"
#include "helperFunctions.hpp"
#include "testDefines.hpp"

static int ret;

uio_info_t* initUio(const std::string& uioName){
    char* uioNameC;

    uioNameC = strdup(uioName.c_str());

    struct uio_info_t *uio;

    uio = uio_find_by_uio_name(uioNameC);
    if (!uio)
    {
        std::cout << "could not find UIO device '" << uioName << "'." << std::endl;
        exit(1);
    }

    ret = uio_open(uio);
    if(ret){ // returnvalue != 0 == error
        std::cout << "could not open UIO device '" << uioName << "': " << std::strerror(errno) << std::endl;
        exit(1);
    }

    return uio;
}

void setupUio(uio_info_t* uio){
    uint32_t magicValue;

    // Read Magic
    std::cout << "1" << std::endl;
    ret = uio_read32(uio, 0, magicAddr, &magicValue);
    std::cout << "2" << std::endl;
    READCHECK(ret, magicAddr, magicValue);
    std::cout << "3" << std::endl;
    // Write to register addr:
    ret = uio_write32(uio, 0, registerAddr, registerAddrValue);
    WRITECHECK(ret, registerAddr, registerAddrValue);
    // Write lpgbt addr:
    ret = uio_write32(uio, 0, lpgbtAddr, lpgbtAddrValue);
    WRITECHECK(ret, lpgbtAddr, lpgbtAddrValue);
}

void closeUio(uio_info_t* uio){
    ret = uio_close(uio);
    if(ret) // -1 on error and errno set
        std::cout << "Failed to close " << uio_get_devname(uio) << ": " << std::strerror(errno) << std::endl;
}

void initTest(uio_info_t* uio, std::vector<uint32_t>values){

    for(auto const& value: values) {
        // Write value to tx_fifo:
        ret = uio_write32(uio, 0, dataTxAddr, value);
        WRITECHECK(ret, dataTxAddr, value);
        // Put it into tx fifo
        ret = uio_write32(uio, 0, gpioAddr, 0x2);
        WRITECHECK(ret, gpioAddr, 0x2);
    }
}

std::vector<uint32_t> readICRxFifo(uio_info_t* uio){
    uint32_t dataRxValue, status_v;
    std::vector<uint32_t>readValues;
    
    status_v = readStatusRegister(uio);
    status_v = isolateBitsRead(status_v, 1, 1); // Isolate to only be the empty flag
    if(status_v)
        return readValues;
    while(!status_v){
        // Read data_rx
        ret = uio_read32(uio, 0, dataRxAddr, &dataRxValue);
        READCHECK(ret, dataRxAddr, dataRxValue);
        readValues.push_back(dataRxValue);
        // Read next byte from rx_fifo
        ret = uio_write32(uio, 0, gpioAddr, 0x1);
        WRITECHECK(ret, gpioAddr, 0x1);

        status_v = readStatusRegister(uio);
        status_v = isolateBitsRead(status_v, 1, 1); // Isolate to only be the empty flag
    }
    return readValues;
}

bool readAndCheckICRxFifo(uio_info_t* uio, std::vector<uint32_t>writtenValues){
    std::vector<uint32_t>readValues = readICRxFifo(uio);
    
    for(auto const& value: writtenValues) {
        if(!(std::find(readValues.begin(), readValues.end(), value) != readValues.end())){ // if value not in readValues
            return false;
        }
    }
    return true;
}

void readLpgbtRegisters(uio_info_t* uio, uint32_t readAmount){
    uint32_t ctrlValue = (readAmount << 16) + 0x4; // 0x4 is the command to read from lpgbt
    ret = uio_write32(uio, 0 , gpioAddr, ctrlValue);
    WRITECHECK(ret, gpioAddr, ctrlValue);
}

void writeLpgbtFifo(uio_info_t* uio){
    // IC tx fifo to lpgbt
    ret = uio_write32(uio, 0, gpioAddr, 0x8);
    WRITECHECK(ret, gpioAddr, 0x8);

}

uint32_t readStatusRegister(uio_info_t* uio){
    uint32_t statusValue;
    // Read status register:
    ret = uio_read32(uio, 0, statusAddr, &statusValue);
    READCHECK(ret, statusAddr, statusValue);
    return statusValue;
}


#if TEST_INTR
    void waitIntr(uio_info_t* uio, int waitDurationMs){
        // Interrupt time
        timeval t;
        t.tv_sec = 0;
        t.tv_usec = waitDurationMs * 1000;
        if(OUTPUTTEST){
            std::cout << "\nWaiting for " << uio_get_devname(uio) << " interrupt..." << std::endl;
            std::cout << "I am not waiting any longer than: " <<  std::dec << int(t.tv_usec)/1000 << std::hex << " ms." << std::endl;
        }
        ret = uio_irqwait_timeout(uio, &t);

        if(ret){ // -1 on error and errno set
            std::cout << "Failed to wait for IRQ for " << uio_get_devname(uio) << ": " << std::strerror(errno) << std::endl;
            throw std::runtime_error("Timed out");
        }
        else{
            if(OUTPUTTEST)
                std::cout << "Interrupt received!\n" << std::endl;
        }
    }

    void enableIntr(uio_info_t* uio, uint32_t interruptEnableValue, uint32_t fieldWidth, uint32_t fieldOffset){
        uint32_t interruptPrevValue;
        ret = uio_enable_irq(uio);
        if(ret) // -1 on error and errno set
            std::cout << "Failed to enable IRQ for " << uio_get_devname(uio) << ": " << std::strerror(errno) << std::endl;

        // Read the old interrupt value and set the bit corresponding to the ready interrupt
        ret = uio_read32(uio, 0, interruptEnableAddr, &interruptPrevValue);
        READCHECK(ret, interruptEnableAddr, interruptPrevValue);
        interruptEnableValue = isolateBitsWrite(interruptEnableValue, fieldWidth, fieldOffset, interruptPrevValue);
        ret = uio_write32(uio, 0, interruptEnableAddr, interruptEnableValue);
        WRITECHECK(ret, interruptEnableAddr, interruptEnableValue);
    }

    void clearIntr(uio_info_t* uio, uint32_t interruptClearValue, uint32_t fieldWidth, uint32_t fieldOffset){
        uint32_t interruptFlagsValue;

        // Read interrupt status
        ret = uio_read32(uio, 0, interruptFlagsAddr, &interruptFlagsValue);
        READCHECK(ret, interruptFlagsAddr, interruptFlagsValue);
        // Clear old interrupts until everything works
        while(isolateBitsRead(interruptFlagsValue, fieldWidth, fieldOffset)) {
            if(OUTPUTTEST)
                std::cout << "Clearing interrupt." << std::endl;
            ret = uio_write32(uio, 0, interruptClearAddr, interruptClearValue); // Clear previous interrupt
            WRITECHECK(ret, interruptClearAddr, interruptClearValue);
            ret = uio_read32(uio, 0, interruptFlagsAddr, &interruptFlagsValue);
            READCHECK(ret, interruptFlagsAddr, interruptFlagsValue);
        }
    }

    void disableIntr(uio_info_t* uio){
        ret = uio_write32(uio, 0, interruptEnableAddr, interruptDisableValue); // Disable interrupts in HDL
        WRITECHECK(ret, interruptEnableAddr, interruptDisableValue);
        ret = uio_disable_irq(uio);
        if(ret) // -1 on error and errno set
            std::cout << "Failed to disable IRQ for " << uio_get_devname(uio) << ": " << std::strerror(errno) << std::endl;
    }
#endif

