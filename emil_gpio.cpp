#include <iostream>
#include <stdexcept>
#include <unistd.h>

#include "libuio.h"
#include "regInfo.hpp"
#include "testFunctions.hpp"
#include "helperFunctions.hpp"

/* new */
#include <stdlib.h>
#include <algorithm>
#include "testDefines.hpp"

// Addresses:
//const uint32_t magicAddr = 0x0;
//const uint32_t gpioAddr = 0x4;


int main(int argc, const char** argv)
{
        std::cout << std::hex;
        //uint32_t magicValue;
        string gpio;
        bool ret;

        const char* uioName = "my_gpio";
		struct uio_info_t *uio = initUio(uioName);

        //setupUio(uio);

        std::cout << "Device: " << uioName << std::endl;

        /* Read Magic
           /  Magic identifier - corresponds to empl
           /  0x656d7049 */

        std::cout << "23"  << std::endl;

        std::cout << "Set gpio to 1 or 0:"  << std::endl;
        std::cin >> gpio;

        if (gpio != 0 || gpio != 1){
                std::cout << "Not valid try again! Type 1 or 0:" <<std::endl;
                std::cin >> gpio;
        }

        if (gpio == 0){
                ret = uio_write32(uio, 0, gpioAddr, 0x00);
                WRITECHECK(ret, gpioAddr, 0x00);
        }

        if (gpio == 1){
                ret = uio_write32(uio, 0, gpioAddr, 0x01);
                WRITECHECK(ret, gpioAddr, 0x01);
        }

        std::cout << "GPIO set to: " << gpio <<std::endl;
        std::cout << "\nProgram finished." << std::endl;
        return 0;
}
