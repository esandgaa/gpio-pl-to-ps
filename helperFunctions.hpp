#pragma once
// Logging related
#define READCHECK(returnValue, addr, value) readCheck(returnValue, addr, value, #addr) // # means preprocessor stringify
#define WRITECHECK(returnValue, addr, value) writeCheck(returnValue, addr, value, #addr) // # means preprocessor stringify
const void readCheck(uint32_t returnValue, uint32_t offset, uint32_t value, std::string regName);
const void writeCheck(uint32_t returnValue, uint32_t offset, uint32_t value, std::string regName);
const void printMainHeader(std::string header);
const void printSubHeader(std::string header);

// Takes value and returns the isolated bits specified by bitWidth and bitOffset. Shifts the bits to 0th position.
uint32_t isolateBitsRead(uint32_t value, uint32_t bitWidth, uint8_t bitOffset);
// Takes and old value and replaces the specified bits defined by value, bitWidth and bitOffset.
uint32_t isolateBitsWrite(uint32_t value, uint32_t bitWidth, uint8_t bitOffset, uint32_t oldValue);